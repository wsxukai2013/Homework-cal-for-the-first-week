class CalculateDemo 
{
	public static void main(String[] args) 
	{
		System.out.println("Please input data");
		calculate(str);
	}
	public static void calculate(String str)
	{ 
        if (str.contains("+")) {
            String[] strs = str.split("\\+");
            double arg1 = Double.valueOf(strs[0]);
            double arg2 = Double.valueOf(strs[1]);
            System.out.println(arg1 + arg2);
        }
        if (str.contains("-")) {
            String[] strs = str.split("\\-");
            double arg1 = Double.valueOf(strs[0]);
            double arg2 = Double.valueOf(strs[1]);
            System.out.println(arg1 - arg2);
        }
        if (str.contains("*")) {
            String[] strs = str.split("\\*");
            double arg1 = Double.valueOf(strs[0]);
            double arg2 = Double.valueOf(strs[1]);
            System.out.println(arg1 * arg2);
        }
        if (str.contains("/")) {
            String[] strs = str.split("\\/");
            double arg1 = Double.valueOf(strs[0]);
            double arg2 = Double.valueOf(strs[1]);
            System.out.println(arg1 / arg2);
        }
	}
}
